# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 23:21:44 2014

Adapted by GMC
@author: Dmitry A. Duev
"""

from pypride.classes import *
from pypride.vintlib import *
from datetime import datetime

''' load input sittings: '''
inp = inp_set('/Users/gmolera/Treball/Repos/pypride/inp.cfg')

'''
#==============================================================================
# obs setup examples
#==============================================================================
'''

''' 1-way ramped Doopler from a deep space S/C'''
# Select start and stop time + interval sampling
t_start = datetime(2022, 2, 11, 9, 20, 0)
t_stop  = datetime(2022, 2, 11, 12, 41, 0)
t_step = 1  # seconds

# input all switches and force calculate delay or calculate Doppler to True
inp_swchs = inp.get_section('all')  # default input switches all False
#inp_swchs['delay_calc'] = True
inp_swchs['doppler_calc'] = True

# Select the object and reference frame
ob = obs(['GEOCENTR', 'HOBART12'], 'MEX', 'M', inp=inp_swchs)
ob.addScan(t_start, t_step, stop=t_stop, freq=8.412e9)

'''
Questions: is the frequency the start of bbc or from the carrier?
What fields need to be passed to obs?
'''

ob.dude = vint_s(ob)

'''
#==============================================================================
# text output
#==============================================================================
'''

# date string
date_string = t_start.strftime("%y%m%d")

# output directory:
out_path = inp.out_path

# get station short names:
stations_short = shname(ob.sta, inp.shnames_cat, inp.shnames_cat_igs)

''' output delays: '''
if ob.inp['delay_calc']:
    # Output file name
    out_txt = f'delay.{ob.source}.{date_string}.{stations_short[0]}{stations_short[1]}.txt'

    with open(f'{out_path}/{out_txt}','w') as f_txt:
        dz = ob.dude.delay
        for jj, _t in enumerate(ob.tstamps):
            ## stack line together for txt output:
            line = '{:4d} {:02d} {:02d} {:02d} {:02d} {:06.3f}  '.\
                    format(_t.year,_t.month,_t.day,\
                           _t.hour,_t.minute,_t.second+_t.microsecond*1e-6) + \
                    '{:23.16e} {:14.7e} {:14.7e} {:14.7e} {:14.7e}\n'.\
                    format(*dz[jj,:5])
            f_txt.write(line)

''' output doppler: '''
if ob.inp['doppler_calc']:
    # it assumes geo1way or bary1way in the Doppler model.
    out_txt = f'doppler.{ob.source}.{date_string}.{stations_short[0]}.txt'

    with open(f'{out_path}/{out_txt}','w') as f_txt:
        dz = ob.dude.doppler
        for jj, _t in enumerate(ob.tstamps):
            ## stack line together for txt output:
            line = f'{_t.year:4d} {_t.month:02d} {_t.day:02d} {_t.hour:02d} {_t.minute:02d} {_t.second + _t.microsecond*1e-6:06.3f}' + \
                f'{dz[jj,0]:23.16e} {dz[jj,:1]:14.7e} {dz[jj,:2]:14.7e} {dz[jj,:3]:14.7e} {dz[jj,:4]:14.7e} \n'

            f_txt.write(line)
