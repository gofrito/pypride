#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2020

calculate the TECS vealues for the ionosphere and troposphere
@author: oasis, gofrito
"""
import argparse
import os
import pandas as pd
import datetime
import numpy as np
import pypride.vintlib as vl
from pypride.vintflib import lagint, pleph
from pypride.classes import ion, constants, inp_set

PYPRIDE_DIR = os.path.dirname(os.path.realpath(__file__))[:-3]

def ion_tec(sta, iono, elv, azi, UT, f_0=None):
    '''
    sta: station class - see classes.py in pypride
    iono: ionosphere class - see classes.py in pypride
    elv: elevation
    azi: azimuth
    UT: UT time

    Calculate ionospheric delay using IGS vertical TEC maps
    '''
    # calculate distance to point J of ray pierce into ionosphere from site
    H: float = 450*1e3 # m - height of ionosphere above R_E as stated in IONEX files
    R_E: float = 6371.0*1e3 # m - Earth's radius from the TEC map

    alpha: float = 0.9782

    lat_geod: float = sta.lat_geod
    lon_gcen: float = sta.lon_gcen
    h_geod: float = sta.h_geod

    UT_tec: list = iono.UT_tec
    fVTEC: list = iono.fVTEC

    #WGS84 Ellipsoid
    a: float = 6378137 # m
    f: float = 1/298.2572235630
    b: float = a * (1 - f) # m
    ec: float = np.sqrt((a**2 - b**2) / a**2)

    R_oscul: float = a * np.sqrt(1.0 - ec**2) / (1 - (ec * np.sin(lat_geod))**2) # m

    source_vec: float = np.array([np.sin(np.pi/2.0-elv)*np.cos(azi),\
                           np.sin(np.pi/2.0-elv)*np.sin(azi),\
                           np.cos(np.pi/2.0-elv)])

    # slanted distance btw the ground and the iono layer
    ds: float = (R_oscul+h_geod)*np.sin(-elv) + \
        0.5 * np.sqrt( (2.0*(R_oscul+h_geod)*np.sin(-elv))**2 - \
        4.0*((R_oscul+h_geod)**2 - (R_E+H)**2) )

    # cart crds of the starting point
    rpt: float = [R_oscul + h_geod, lat_geod, lon_gcen]
    r0: float = vl.sph2cart(rpt)

    # cart crds of the ionospheric pierce point
    r1: float = r0 + ds*source_vec

    # lat/long of the pierce point
    rlalo: float = vl.cart2sph(r1)
    lon: float = 180 * rlalo[2] / np.pi
    lat: float = 180 * rlalo[1] / np.pi

    # find closest epoch in TEC data:
    # easy case - UT is in UT_tec
    n0 = np.searchsorted(UT_tec, UT)
    if UT in UT_tec:
        # only need to interpolate TECz to the Lat/Lon of the pierce point
        TEC_z = float(fVTEC[n0](lon, lat))
    else:
        # else take 4 consecutive epochs and interpolate:
        N_epoch: int = len(fVTEC)
        if n0==1 or n0==0:
            nl: int = 0
            nr: int = 4
        elif n0==N_epoch-2 or n0==N_epoch-1:
            nl: int = N_epoch-4
            nr: int = N_epoch
        else:
            nl: int = n0-2
            nr: int = n0+2
        TEC_z = []
        for nn in range(nl,nr):
            TEC_z.append(float(fVTEC[nn](lon, lat)))
        # interpolate zenith TECz to the epoch of observation
        TEC_z = np.interp(UT, UT_tec[nl:nr], TEC_z)

    # calculate slanted TEC
    TEC: float = TEC_z / np.cos(np.arcsin((R_oscul+h_geod)*np.sin(alpha*(np.pi/2.0-elv))/(R_E+H)))
    TEC_tecu: float = 0.1*TEC # in TEC units

    return TEC_tecu

def calctec(scan, sta, ip_tecs):
    '''
    scan:
    sta_r: station class - see classes.py in pypride
    sta_t: station class - see classes.py in pypride
    ip_tecs:

    Calculate TEC values
    '''
    receivers = ['Mh', 'Mc', 'Mt', 'Nt', 'Wz', 'Ys', 'Pu', 'On', \
            'Hh', 'Sh', 'Km', 'Ur', 'Ht', 'Sv', 'Zc', 'Bd', 'T6', \
            'Ww', 'Ho', 'Hb', 'Yg', 'Ke', 'Wa', 'Wd', 'Wn', 'Ku', 'Cd']

    # scan line with all measurement values
    HH, MM = scan.Time.split(':')
    yy, mm, dd = scan.DOY.split('.')
    t_obs: float = (int(HH)*3600 + int(MM)*60 + scan.Duration/2)/86400
    t = datetime.datetime(int(yy), int(mm), int(dd)) + datetime.timedelta(t_obs)

    ''' load vtec maps '''
    if scan.DownIonos == 30.0 or scan.UpIonos == 30.0 or scan.TEC == 1.0:
        iono = ion(t, t, inp)
    if scan.DownIonos == 30.0:
        st = sta_r[receivers.index(scan.Station)]

        el: float = scan.Elevation * np.pi / 180
        if scan.Azimuth > 180:
            az: float = (scan.Azimuth - 360) * np.pi / 360
        else:
            az: float = scan.Azimuth * np.pi / 180

        down_ionos = ion_tec(st, iono, el, az, t_obs)
        scan.DownIonos = f'{down_ionos:.1f}'

    if scan.UpIonos == 30.0:
        st = sta_t[int(scan.GroundStation)-1] # transmittimg station
        # use planetary ephems
        mjd = vl.mjuliandate(t.year, t.month, t.day)
        UTC = (t.hour + t.minute/60.0 + t.second/3600.0)/24.0

        JD = mjd + 2400000.5
        TAI, TT = vl.taitime(mjd, UTC)
        tags = ['yy','mm','dd','mjd','x','y','ut1','lod','dx','dy','xer','yer','ut1err','loderr','dxerr','dyerr']
        eop_par = pd.read_csv(inp['cat_eop'], sep="\s+", header=None, skiprows=15, skipfooter=0, engine='python', names=tags)

        eops = np.zeros((7,7)) # +/- 3 days
        for kk in range(7):
            eops[kk, 0] = eop_par[eop_par.mjd == mjd - 3 + kk].mjd
            eops[kk, 1] = eop_par[eop_par.mjd == mjd - 3 + kk].ut1
            eops[kk, 2] = eop_par[eop_par.mjd == mjd - 3 + kk].ut1 - vl.nsec(eops[kk, 0])
            eops[kk, 3] = eop_par[eop_par.mjd == mjd - 3 + kk].x
            eops[kk, 4] = eop_par[eop_par.mjd == mjd - 3 + kk].y
            eops[kk, 5] = eop_par[eop_par.mjd == mjd - 3 + kk].dx
            eops[kk, 6] = eop_par[eop_par.mjd == mjd - 3 + kk].dy

        UT1, eop_int = vl.eop_iers(mjd, UTC, eops)
        CT, dTAIdCT = vl.t_eph(JD, UT1, TT, st.lon_gcen, st.u, st.v)

        # Earth:
        rrd = pleph(JD+CT, 3, 12, inp['jpl_eph'])
        # It doesn't need reshaping
        earth = np.asarray(rrd).T * 1e3

        # Venus/Mars/Jupiter
        if source.lower()=='vex':
            rrd = pleph(JD+CT, 2, 12, inp['jpl_eph'])
        elif source.lower()=='mex':
            rrd = pleph(JD+CT, 4, 12, inp['jpl_eph'])
        elif source.lower()=='jun' or source.lower()=='jui':
            rrd = pleph(JD+CT, 5, 12, inp['jpl_eph'])

        planet = np.asarray(rrd).T * 1e3
        lt = np.linalg.norm(planet[:,0] - earth[:,0]) / const.C
        dt = 3 * datetime.timedelta(seconds=lt) # 2-way LT

        if tec_uplink=='planet':
            # 3 LTs ago (2LTs - signal round trip + another LT )
            CT -= dt.total_seconds()/86400.0
            _, eop_int = vl.eop_iers(mjd, UTC-2.0*lt/86400.0, eops)
            ## Earth:
            rrd = pleph(JD+CT, 3, 12, inp['jpl_eph'])
            earth = np.asarray(rrd).T * 1e3
            # Earth's acceleration in m/s**2:
            v_plus = np.array(pleph(JD+CT+1.0/86400.0, 3, 12, inp['jpl_eph'])[1])
            v_minus = np.array(pleph(JD+CT-1.0/86400.0, 3, 12, inp['jpl_eph'])[1])
            a = (v_plus - v_minus) * 1e3 / 2
            a = np.array(np.matrix(a).T)
            earth = np.hstack((earth, a))
            ## Sun:
            rrd = pleph(JD+CT, 11, 12, inp['jpl_eph'])
            sun = np.asarray(rrd).T * 1e3
            ## Moon:
            rrd = pleph(JD+CT, 10, 12, inp['jpl_eph'])
            moon = np.asarray(rrd).T * 1e3
            # Venus/Mars
            if source.lower()=='vex':
                rrd = pleph(JD+CT, 2, 12, inp['jpl_eph'])
            elif source.lower()=='mex':
                rrd = pleph(JD+CT, 4, 12, inp['jpl_eph'])
            elif source.lower()=='jun':
                rrd = pleph(JD+CT, 5, 12, inp['jpl_eph'])

            planet = np.asarray(rrd).T * 1e3
            r2000 = vl.ter2cel(t - dt, eop_int, dTAIdCT, 'iau2000')
            st = vl.dehanttideinel(st, t - dt, earth, sun, moon, r2000)
            st = vl.hardisp(st, t - dt, r2000)
            st = vl.poletide(st, t - dt, eop_int, r2000)
            st.j2000gp(r2000)

            r = planet[:, 0] - (st.r_GCRS + earth[:, 0])
            ra: float = np.arctan2(r[1], r[0]) # right ascention
            dec: float = np.arctan(r[2] / np.sqrt(r[0]**2 + r[1]**2)) # declination
            if ra < 0:
                ra += 2.0*np.pi
            K_s = np.array([np.cos(dec)*np.cos(ra), np.cos(dec)*np.sin(ra), np.sin(dec)])

            az, el = vl.aber_source(st.v_GCRS, st.vw, K_s, r2000, earth)
            tec = ion_tec(st, iono, el, az, (t_obs - 2 * lt / 86400))

        elif tec_uplink=='sc':
            eph = vl.load_sc_eph(sou_type, source, t, t, inp)
            # lt to station in seconds at t_obs
            lt, _, _ = st.LT_radec_bc(eph.bcrs[0], eph.CT, JD, UTC, inp['jpl_eph'])
            # az/el 2LT ago (another LT is accounted for internally)
            r2000 = vl.ter2cel(t, eop_int, dTAIdCT, 'iau2000')
            az, el = st.AzEl2(eph.gtrs, eph.UT, JD, \
                                UTC - 2.0*lt/86400.0, inp['jpl_eph'])
            tec = ion_tec(st, iono, el, az, (t_obs - 2.0*lt/86400.0))

        scan.UpIonos = f'{tec:.1f}'

    if scan.TEC == 1.0:
        orb_phase: float = scan.SOT + scan.STO
        ip_tec, _ = lagint(4, np.array(ip_tecs.OrbPhase), np.array(ip_tecs.OneWayTEC), orb_phase)
        ips_tec = float(ip_tec)*(1+880/749)
        scan.TEC = f'{ips_tec:.1f}'

    return scan

# create parser
parser = argparse.ArgumentParser()

# optional arguments
parser.add_argument('-s', '--spacecraft', type=str, choices=['vex', 'mex', 'jun', 'jui', 'bco'], help='spacecraft')
parser.add_argument('-u', '--uppoint', type=str, choices=['sc', 'planet'],
                default='planet',
                help='where to point when calculating TEC on uplink:'+\
                ' \'planet\' to point at the S/C host planet (default),'+\
                ' \'sc\' to point at the S/C')
parser.add_argument('-i', '--ionomodel', type=str, choices=['igs', 'igr'],
                default='igs',
                help='IGS\' ionospheric TEC model to use: final or rapid (default)')
parser.add_argument('-p', '--parallel', action='store_true', help='run computation in parallel mode')

# positional argument
parser.add_argument('inpFile', type=str, help="input ScintObsSummary table")

args = parser.parse_args()
scint_table_file: str = args.inpFile

# which spacecraft?
if args.spacecraft=='vex':
    source: str = 'vex'
elif args.spacecraft=='mex':
    source: str = 'mex'
elif args.spacecraft=='jun':
    source: str = 'jun'
elif args.spacecraft=='jui':
    source: str = 'jui'
elif args.spacecraft=='bco':
    source: str = 'bco'
else:
    # try to guess:
    if 'vex' in scint_table_file.lower():
        source: str = 'vex'
    elif 'mex' in scint_table_file.lower():
        source: str = 'mex'
    elif 'jun' in scint_table_file.lower():
        source: str = 'jun'
    elif 'bco' in scint_table_file.lower():
        source: str = 'bco'
    elif 'jui' in scint_table_file.lower():
        source: str = 'jui'

    else:
        raise Exception('Spacecraft not set; failed to guess.')

# where to point uplink?
if args.uppoint=='sc':
    tec_uplink: str = 'sc'
elif args.uppoint=='planet':
    tec_uplink: str = 'planet'
else:
    # let's do it quickly by default
   tec_uplink: str = 'planet'

inp = inp_set(f'{PYPRIDE_DIR}inp.cfg')

inp = inp.get_section('all')
const = constants()

if args.ionomodel:
    inp['ionomodel'] = args.ionomodel

PYSCTRACK_DIR = inp['pysctrack_path']
PYSCTRACK_CAT = os.path.join(PYSCTRACK_DIR,'cats/',)

# Total number of antennas 27
receivers = ['METSAHOV', 'MEDICINA', 'MATERA', 'NOTO', 'WETTZELL', \
            'YEBES40M', 'PUSHCHIN', 'ONSALA60', 'HARTRAO', \
            'SESHAN25', 'KUNMING', 'URUMQI', 'HART15M', \
            'SVETLOE', 'ZELENCHK', 'BADARY', 'TIANMA65', 'WARK12M',\
            'HOBART26', 'HOBART12', 'YARRA12M', 'KATH12M',\
            'WARK30M', 'WETTZ13S', 'WETTZ13N', 'KVNUS', 'CEDUNA']

# Total number of antennas 12
transmitters = ['NWNORCIA', 'CEBREROS', 'MALARGUE', 'TIDBIN64', 'DSS35',\
               'DSS45', 'DSS34', 'DSS65', 'DSS63', 'DSS14', 'DSS15',\
               'JIAMUSI']

# Here
recv_short = vl.shname(receivers, inp['shnames_cat'], inp['shnames_cat_igs'])
tran_short = vl.shname(transmitters, inp['shnames_cat'], inp['shnames_cat_igs'])

''' load cats '''
# last argument is dummy
sou_type = 'S'
_, sta_r, _ = vl.load_cats(inp, source, sou_type, receivers, \
                        datetime.datetime.now())
_, sta_t, _ = vl.load_cats(inp, source, sou_type, transmitters, \
                        datetime.datetime.now())

''' calculate site positions in geodetic coordinate frame
+ transformation matrix VW from VEN to the Earth-fixed coordinate frame '''
for st_r in sta_r:
    st_r.geodetic(const)
for st_t in sta_t:
    st_t.geodetic(const)

''' load IP TEC table '''
if source == 'vex':
    f_iptec: str = 'TecVenus.NFSn720.txt'
elif source == 'mex':
    f_iptec: str = 'TecMars.NFSn720.txt'
elif source == 'jui':
    f_iptec: str = 'TecJuice.NFSn720.txt'
else:
    print('TEC table not implemented for this planet/spacecraft')

''' Parse Simulated TEC Table '''
tags: list = ['OrbPhase','SOT','DistanceAU','OneWayTEC','FastWindTEC','SlowWindTEC']
ip_tecs = pd.read_csv(f'{PYSCTRACK_CAT}{f_iptec}', sep="\s+", header=None, skiprows=0, skipfooter=0, engine='python', names=tags)

''' Parse Scint Table '''
tags = ['Observation','Run','Scan','Station','DOY','Time',\
       'Duration','RAh','RAm','RAs','DEd','DEm','DEs','Azimuth','Elevation','Longitude','Latitude',\
       'Distance','SOT','STO','Scint','SysNoise','SciSlope','ErrSlope','PeakSPD','Noise',\
       'Dnoise','CarrierSNR','SolarAct','GroundStation','DownIonos','UpIonos','TEC']

'''
Input method:
 1- Standard Sergeis' Scintillation table
 2- Phases summary generated by scintAnalaysis
'''
inp_file = scint_table_file.split('/')[-1]
if inp_file[0:5] == 'Scint':
    scint_table = pd.read_csv(scint_table_file, sep="\s+", header=None, skiprows=5, skipfooter=70, engine='python', names=tags)
elif inp_file[0:5] == 'Phase':
    scint_table = pd.read_csv(scint_table_file, sep="\s+", header=None, skiprows=1, skipfooter=0, engine='python', names=tags)
else:
    print(f'Unrecognized file {inp_file}')

dates = []
for scan in range(scint_table.shape[0]):
    yy,mm,dd = scint_table.DOY[scan].split('.')
    dates.append(datetime.datetime(int(yy),int(mm),int(dd)))
dates = np.unique(dates)

print('Fetching ionospheric data...')
#tec_model = 'igs' # final 'igs' or rapid 'igr' IGS solution
tec_model = inp['iono_model']
for t in dates:
    vl.doup(False, inp['do_ion_calc'], inp['cat_eop'], inp['meteo_cat'], inp['ion_cat'], t, t, tec_model)

''' iterate over records in scint table '''
print('Computing TECs...')
fd = open(f'{PYSCTRACK_CAT}scint_template.txt','r')

if inp_file[0:5] == 'Scint':
    fo = open(f'{scint_table_file[:-5]}e.txt','w')
elif inp_file[0:5] == 'Phase':
    fo = open(f'{scint_table_file[:-4]}.re.txt','w')

header: str = ''
footer: str = ''
for ip in range(5):
    header += fd.readline()

print('Printing updated table...')
# print header:
fo.write(header)

# print entries one by one
for scan in range(scint_table.shape[0]):
    print(scint_table.shape[0]-scan, 'records to go')
    scint_entry = scint_table.loc[scan,:].copy()
    line = calctec(scint_entry, sta_r, ip_tecs)
    print_out = (f'{line.Observation:5d}{line.Run:4d}{line.Scan:4d}{line.Station:>4}' + \
        f'{line.DOY:>12}{line.Time:>7}{line.Duration:6d}{line.RAh:4d}{line.RAm:3d}' + \
        f'{line.RAs:5.1f}{line.DEd:5d}{line.DEm:3d}{line.DEs:5.1f}{line.Azimuth:7.1f}' + \
        f'{line.Elevation:6.1f}{line.Longitude:7.1f}{line.Latitude:7.1f}{line.Distance:8.4f}' + \
        f'{line.SOT:7.2f}{line.STO:7.2f}{line.Scint:9.3f}{line.SysNoise:7.3f}' + \
        f'{line.SciSlope:8.3f}{line.ErrSlope:6.3f}{line.PeakSPD:9.2f}{line.Noise:8.2f}' + \
        f'{line.Dnoise:8.2f}{line.CarrierSNR:8.0f}{line.SolarAct:5d}{line.GroundStation:4d}' + \
        f'{line.DownIonos:>8}{line.UpIonos:>8}{line.TEC:>8}\n')
    fo.write(print_out)

# Read each line from the template file
for ip in range(70):
    footer += fd.readline()

# print footer
fo.write(footer)

# Close files
fd.close()
fo.close()
