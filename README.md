# pypride

Forked version of the pypride-dev repository from Dima, mirrored and imported in GitLab

## Installation

The preferred method to install the package is using a packaging tool like flit

Simply run:
```
flit install
```

However, at the moment admint2 is still in fortran, so you need to also run distutils to run the f2py script and install that library. 

```
f2py -m admint2 -c pypride/admint2.f90
```

```bash
python setup.py install
```

## Data

Pypride installation creates a new directory in your home called **.pypride** in which all the catalogues and other files are stored and used directly from the scripts.
